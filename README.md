# Peptide Retrieval Software [PepRS] #

### What is this repository for? ###

* Software suite to analyze phage display sequences from phD-family phage libraries sequenced by the method of Liu et al. [See below for citation]
* Software is tested for Ubuntu and Linux systems (including Mac OSX)
* All dependencies and test data are included.  You should be able to download the program and get running.

### How do I get set up? ###
Clone or download this repository, then unzip it in the desired directory.

From command line, change directory into your peprs repository:

* cd /path/to/peprs

To run the program, type the following, setting parameters as appropriate:

* ./peprs.sh -d /path/to/fastq/input/files -p prefix -s /path/to/save/files -t librarytype -f filterdepth -u unselected_library

Usage of the program requires you to enter parameters for each of the following command-line flags, explained below:

* [-h help] you are reading the "help" message now. 

* [-p prefix] Prefix is the sample name.  Prefixes cannot contain underscores. Input filenames must follow the naming convention "prefix_*.fastq.gz"

* [-t type] phD library type.  Recognized Library Types are phD-12, phD-7, and phD-C7C

* [-s savepath] absolute/path/to/directory to save your data.  This directory will be created if it doesn't exist already

* [-d datapath] absolute/path/to/directory where your gzipped input fastq files reside

* [-u unselected] Prefix of the unselected library, if you want to do a comparison of your library to an unselected library.  Set to "NO" if you don't want to compare your library to an unselected population.  Note that the analysis files for the unselected population MUST be in the SAVEPATH where you are writing your current analysis

* [-f filter_depth] In your final fasta file, filter away all sequences that do not have this number of representatives or more, as well as those with forbidden characters (stopgains).  Set to 0 to disable filtering on the number of representatives.

### Input Files ##

This program takes demultiplexed fastq files, one for each library being analyzed.  Fastq files must be gzipped and follow the naming convention "prefix_*.fastq.gz".  This program has been tested using Illumina sequencing data but may work for other sequencers.

### Output Files ##

The specified save directory will be populated with files with the following extensions as output for each library analyzed:

* *.QC_metrics.txt = statistics about the number of sequences in a sequencing run and the fraction of sequences passing filter

* *.phage_display_histogram_protein.fa = multifasta file with all protein sequences included in the library, named ">relativeabundance_numbersequences_proteinsequence"

* *.sortedbyrank.txt = list of sequences, ordered by their relative sequence read counts, and compared to relative abundance in the unselected library (if specified)

* *.sortedbyenrichment.txt = list of sequences, ordered by their relative enrichment compared to the unselected library.  This file is only generated if an unselected library is specified.

* *.phage_display_rank_filtered.fa = multifasta file with all valid protein sequences (no premature stops) occurring above the specified read count, ordered by relative abundance, and named ">abundancerank_numbersequences_proteinsequence"

* *.phage_display_enrichmented_filtered.fa = multifasta file with all valid protein sequences (no premature stops) occurring above the specified read count, ordered by their relative enrichment compared to the unselected library, and named ">enrichmentrank_numbersequences_proteinsequence".  This file is only generated if an unselected library is specified.

### Test Data ###

Test data is included with this repository.  To run the program with the test data, run the following commands from the command line / terminal:

* cd /path/to/your/copy/of/peprs

* ./peprs.sh -d ./test_files -p Unselected -s ./testoutput -t phD-12 -f 0 -u NO -h 

* ./peprs.sh -d ./test_files -p M2pepR2-4 -s ./testoutput -t phD-12 -f 4 -u Unselected -h 

This should generate an output directory called "testoutput" within your local peprs directory, 


### Suggested Workflow ###
* The typical workflow for this program is to analyze the unselected library FIRST, using the flag "-u NO".  
* After analyzing the unselected library is complete, you can then run all your experimental libraries and specify the prefix of this unselected library under the "-u" flag in order to enable their normalization and compare them to the unselected state. 
* If you are using a multi-core computer, you can run multiple analyses in parallel (after analysis of the unselected library) by entering each command with an "&" at the end.

### Notes ###
* The test data is very small and will run very quickly.  Your data may take substantially longer to run.  Be patient!  
* For large data sets, it is recommended that you run the program using "nohup" (ie - nohup ./peprs.sh <appropriate flags>)
* Flags are case sensitive.  Be careful what you type!

### How do I cite this program? ###
Please cite:

Efficient identification of peptide targeting ligands by phage display and next-generation sequencing. Gary W. Liu, Brynn R. Livesay, Nataly A. Kacherovsky, Maryelise Cieslewicz, Emi Lutz, Adam Waalkes, Michael C. Jensen, Stephen J. Salipante, Suzie H. Pun. Bioconjug Chem. 2015 Aug 19;26(8):1811-7

### Who do I talk to? ###
* see enclosed license agreement
* check error messages printed by the program for hints at the problem
* contact stevesal@uw.edu for details