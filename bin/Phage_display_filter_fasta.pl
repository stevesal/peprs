#!/usr/bin/perl

#sequence name prints in format of ">rank_abundance_sequence"

#echo "Sequence    Selected_fraction Unselected_fraction   Ratio_change_in_selected    Sequence_count_selected Sequence_count_unselected   Total_sequence_count_selected: $SELECTEDCOUNT   Total_sequence_count_unselected: $UNSELECTEDCOUNT Unselected_Library: $UNSELECTED" > $SAVEPATH/$PFX.sortedbyenrichment.txt


my $depth = $ARGV[0];
my $input = $ARGV[1];
my $outputfasta = $ARGV[2];


open( output, ">$outputfasta");
close( output); 

open( output, ">>$outputfasta");

open (INPUT, "$input");


my $rankcounter = 0;
my $linetoprint = ();
while (<INPUT>) 
{
my $line = $_;
chomp $line;

@linearray = split( ' ', $line);

#print "$linearray[0]  $linearray[4]\n";


#do not print this sequence if it has a forbidden character "_" or "X" or does not satisfy our cutoff read detph

unless ($line =~ /^Sequence/ or $linearray[4] < $depth or $linearray[0] =~ /_/ or $linearray[0] =~ /X/)
{

$linetoprint="\>".$count."_".$linearray[4]."_".$linearray[0];

print output "$linetoprint\n$linearray[0]\n";
$linetoprint = ();

}

$count ++;

}

close INPUT;
close output;

exit;