#!/usr/bin/perl

#use R1. only sequences with both adapters included.  

#program to take a FASTA files of aptamers (illumina adaptor trimmed) and spit out just the aptamer sequences themselves.

#v2 update removes flanking sequnces from phd-c7c (CGA ACA NNN7 ACG)

#phage display casette is PHD-12
#read from custom illumina sequencing primers as follows
#(N)36 GGT GGA GGT TCG GCC GAA . . . .

#match against first 10 nucleotides of the downstream sequence
my $downstreamseq="GGTGGAGGTT";

my $inputfasta = $ARGV[0];
my $outputfasta = $ARGV[1];
my $outputlist = $ARGV[2];

my $randommerlength = $ARGV[3];
#here are the randommerlengths to use:
#phd-12 = 36
#phD-7 = 21
#phD-C7C = 30 (includes leader and tailer sequences)



#my $randommerlength = 36;


my $substringlength = length($downstreamseq);


open( output, ">$outputfasta");
close( output); 

open( output, ">>$outputfasta");

open( output2, ">$outputlist");
close( output2); 

open( output2, ">>$outputlist");

open (INPUT, "$inputfasta");

my $headerline = ();
my $apatamersequence =();

my $fastq_linecounter = 0;

my $tempreverse = ();

while (<INPUT>) 
{
my $line = $_;
chomp $line;

if ($fastq_linecounter == 0){$headerline = ">".$line; $fastq_linecounter ++; }

elsif ($fastq_linecounter == 1)
    {
    
#print "linecounter 1 passed = $line\n";
$fastq_linecounter ++;    

    #proceed only if the line matches expected sequence structure
    my $match = substr($line, ($randommerlength), $substringlength);
    
#print "$match\n";    

     if ( $match eq $downstreamseq)
            {
        
            my $randomer = substr($line, 0, ($randommerlength) );
            
            #special provision for phd-c7c - remove leading 6 nucs and trailing 3 nucs
            my $randommertrimmed = ();
            if ($randommerlength == 30)
            {
            $randommertrimmed = substr($randomer, 6, 21 );
            $randomer = $randommertrimmed;
            
            }
            
            
            print output "$headerline\n$randomer\n";
     
            print output2 "$randomer\n";
            }
     
    $headerline = ();
    }
        
        

    
elsif ($fastq_linecounter == 2){$fastq_linecounter ++; }#print "linecounter 2 passed = $line\n";}
elsif ($fastq_linecounter == 3){$fastq_linecounter = 0; }#print "linecounter 3 passed, reset = $line\n";}

        
           

}
close INPUT;
close output;

exit;