#!/bin/bash
# Stephen Salipante

#example for unselected population
#nohup ./peprs.sh -d ./test_files -p Unselected -s ./testoutput -t phD-12 -f 0 -u NO -h &
#nohup ./peprs.sh -d ./test_files -p M2pepR2-4 -s ./testoutput -t phD-12 -f 4 -u Unselected -h &


#relative filepaths
SCRIPT_DIR="./bin";


echo "### Peptide Retrieval Software [PepRS] ###"
echo "This project is free for use by Academic users. Please see enclosed license agreement for details and conditions of use."
echo ""
echo "Run program with -h flag for instructions"


#variables
#PFX=$1;
#RANDOMMERLENGTH=$2
#SAVEPATH=$3;
#DATAPATH=$4;

#here are the randommerlengths to use:
#phD-12 = 36
#phD-7 = 21
#phD-C7C = 30 (includes leader and tailer sequences)

usage() {

cat << EOF

Usage as follows:
[-h help] you are reading the "help" message now.  Aren't you lucky?
[-p prefix] Prefix is the sample name.  Prefixes cannot contain underscores. Input filenames must follow the naming convention "prefix_*.fastq.gz"
[-t type] phD library type.  Recognized Library Types are phD-12, phD-7, and phD-C7C
[-s savepath] absolute/path/to/directory to save your data.  This directory will be created if it doesn't exist already
[-d datapath] absolute/path/to/directory where your gzipped input fastq files reside
[-u unselected] Prefix of the unselected library, if you want to do a comparison of your library to an unselected library.  Set to "NO" if you don't want to compare your library to an unselected population.  Note that the analysis files for the unselected population MUST be in the SAVEPATH where you are writing your current analysis
[-f filter_depth] In your final fasta file, filter away all sequences that do not have this number of representatives or more, as well as those with forbidden characters (stopgains).  Set to 0 to disable filtereing on the number of representatives.
EOF
}

#colon after paramter is a required parameter
while getopts "p:t:s:d:f:u:h" flag ; do
        case $flag in
                h ) usage;;
                p ) PFX=$OPTARG;;
                t ) TYPE=$OPTARG;;
                s ) SAVEPATH=$OPTARG;;
                d ) DATAPATH=$OPTARG;;
                u ) UNSELECTED=$OPTARG;;
                f ) FILTER=$OPTARG;;
                * ) usage;;     
        esac
done


echo ""
echo "Selected run parameters are:"
echo PREFIX = "${PFX}"
echo LIBRARY TYPE = "${TYPE}"
echo SAVE PATH = "${SAVEPATH}"
echo DATA PATH = "${DATAPATH}"
echo FILTER DEPTH = "${FILTER}" #Number of sequences "ALL" if you want to include all sequences
echo UNSELECTED LIBRARY = "${UNSELECTED}" #"NO" if you do not want to do this
echo ""

#error checking for lost variables
if [[ $TYPE = "" ]]; then
    echo "ERROR: LIBRATRY TYPE not set.  All parameters must be specified to run the analysis."
    exit 1  
fi
if [[ $SAVEPATH = "" ]]; then
    echo "ERROR: SAVE PATH not set.  All parameters must be specified to run the analysis."
    exit 1  
fi

if [[ $PFX = "" ]]; then
    echo "ERROR: PREFIX not set.  All parameters must be specified to run the analysis."
    exit 1  
fi
if [[ $DATAPATH = "" ]]; then
    echo "ERROR: DATA PATH not set.  All parameters must be specified to run the analysis."
    exit 1  
fi
if [[ $FILTER = "" ]]; then
    echo "ERROR: FILTER DEPTH not set.  All parameters must be specified to run the analysis."
    exit 1  
fi

if [[ $UNSELECTED = "" ]]; then
    echo "ERROR: UNSELECTED LIBRARY not set.  All parameters must be specified to run the analysis."
    exit 1  
fi

#error checking for library type
FOUND=0
if [[ $TYPE = "phD-12" ]]; then
    FOUND=1
fi
if [[ $TYPE = "phD-7" ]]; then
    FOUND=1
fi

if [[ $TYPE = "phD-C7C" ]]; then
    FOUND=1
fi

if [[ $FOUND = 0 ]]; then
    echo "ERROR: Specified Library Type is not recognized."
    echo "Recognized Library Types are phD-12, phD-7, and phD-C7C."
    echo "Specify exactly one of these Library types (parameter is case sensitive and punctuation sensitive)."
    echo "Exiting program"
    exit 1 
fi

# begin analysis

echo -n "Initiating analysis ";
date +"%D %H:%M" 

#translate phage display type to randommerlength
if [[ $TYPE = "phD-12" ]]; then
RANDOMMERLENGTH=36
fi
if [[ $TYPE = "phD-7" ]]; then
RANDOMMERLENGTH=21
fi
if [[ $TYPE = "phD-C7C" ]]; then
RANDOMMERLENGTH=30
fi



mkdir -p $SAVEPATH


R1=$(echo $DATAPATH/${PFX}_*.fastq.gz)

cp $R1 $SAVEPATH/$PFX.R1.fastq.gz

gunzip $SAVEPATH/$PFX.R1.fastq.gz

echo -n "Trimming Reads ";
date +"%D %H:%M" 

perl $SCRIPT_DIR/Phage_Display_filtertrim_fastq_V2.pl $SAVEPATH/$PFX.R1.fastq $SAVEPATH/$PFX.phage_display_seqs.fa $SAVEPATH/$PFX.phage_display_list.txt $RANDOMMERLENGTH


echo -n "Calculating QC metrics ";
date +"%D %H:%M" 

#calculate QC metrics - percent reads passing filter, etc.
PASSING=$(wc -l $SAVEPATH/$PFX.phage_display_list.txt | sed -e 's/^[ \t]*//' | cut -d " " -f 1)
TOTAL_ALL=$(wc -l $SAVEPATH/$PFX.R1.fastq | sed -e 's/^[ \t]*//' | cut -d " " -f 1)
TOTAL=$(( $TOTAL_ALL / 4))

FRAC_PASS=$( echo $PASSING / $TOTAL | bc -l )
FRAC_FILT=$(echo 1 - $FRAC_PASS | bc -l )

echo "Sample_ID    Total_reads    Passing_reads    Fraction_passing   Fraction_filtered" > $SAVEPATH/$PFX.QC_metrics.txt;
echo "$PFX    $TOTAL    $PASSING    $FRAC_PASS   $FRAC_FILT" >> $SAVEPATH/$PFX.QC_metrics.txt;


echo -n "Translating to protein ";
date +"%D %H:%M" 

#7) translate the redundant list of phage display sequences to protein.
perl $SCRIPT_DIR/Phage_display_translate_to_protein.pl $SAVEPATH/$PFX.phage_display_list.txt $SAVEPATH/$PFX.phage_display_list_protein.txt

echo -n "Calculating protein histogram ";
date +"%D %H:%M" 

#8) Get unique sequences out and count ocurrences, then sort by most to least abundance
sort $SAVEPATH/$PFX.phage_display_list_protein.txt | uniq -c > $SAVEPATH/$PFX.phage_histogram_protein.txt

sort -k1 -r -n $SAVEPATH/$PFX.phage_histogram_protein.txt > $SAVEPATH/$PFX.phage_histogram_sorted_protein.txt
rm $SAVEPATH/$PFX.phage_histogram_protein.txt

#5) convert  histogram to a fasta with each sequence named by uniqueID_abundance_seqname
perl $SCRIPT_DIR/Phage_display_histogram_to_fasta.pl $SAVEPATH/$PFX.phage_histogram_sorted_protein.txt $SAVEPATH/$PFX.phage_display_histogram_protein.fa


echo -n "Cleaning up files ";
date +"%D %H:%M" 

#cleanup big files
rm -f  $SAVEPATH/$PFX.R1.fastq
rm -f  $SAVEPATH/$PFX.phage_display_seqs.fa
rm -f  $SAVEPATH/$PFX.phage_display_list.txt


#============ compare to unselected, if this is specified, or reformat if not

echo -n "Normalizing and formatting results ";
date +"%D %H:%M" 

#cheat to use some old code by renaming a variable
UNSELECTEDPROT=$UNSELECTED

#append prefix of unselected and selected files to yield the whole filenames, if appropriate
if [ "$UNSELECTEDPROT" != "NO" ]
then
UNSELECTEDPROT=$SAVEPATH/$UNSELECTED.phage_histogram_sorted_protein.txt
fi

SELECTEDPROT=$SAVEPATH/$PFX.phage_histogram_sorted_protein.txt

#get count of read counts in unselected file, if supplied.  if not supplied, data frame will remain empty
UNSELECTEDCOUNT=0

if [ "$UNSELECTEDPROT" != "NO" ]
then

while read line
do

    UNSELCTEDSEQUENCE=$(echo $line | tr -s ' ')
    unselectedarray=()
    IFS=' ' read -a unselectedarray <<< "$UNSELCTEDSEQUENCE"
    
    UNSELECTEDCOUNT=$((UNSELECTEDCOUNT + ${unselectedarray[0]}))


done < $UNSELECTEDPROT
fi

#make basename for saving these files
SAVEFILE=$SAVEPATH/$PFX.corrected.txt

echo -n "" > $SAVEFILE


#compare these sequences to unselected, if indicated
#get count of sequences in the selected file
SELECTEDCOUNT=0
while read line
do

    SELCTEDSEQUENCE=$(echo $line | tr -s ' ')
    selectedcountarray=()
    IFS=' ' read -a selectedcountarray <<< "$SELCTEDSEQUENCE"
    
    SELECTEDCOUNT=$((SELECTEDCOUNT + ${selectedcountarray[0]}))


done < $SAVEPATH/$PFX.phage_histogram_sorted_protein.txt

#now go through selected file and calculate the fraction of reads represented by each sequence

#read through each line of the selected file and compare to unselected
while read line
do
    ENRICHMENT=0
    selectedarray=();
    SELCTEDSEQUENCE=$(echo $line | tr -s ' ')
    IFS=' ' read -a selectedarray <<< "$SELCTEDSEQUENCE"
    
    MATCH=();
    
    if [ "$UNSELECTEDPROT" != "NO" ]
    then
      MATCH=$(grep ${selectedarray[1]}  $UNSELECTEDPROT | tr -s ' ')
    fi
#Calculate fractions
      FRACTIONSELECTED=$( echo ${selectedarray[0]} / $SELECTEDCOUNT | bc -l )

    #IF NO MATCH is found
    if [ -z "$MATCH" ];
    then
      
      echo "${selectedarray[1]} $FRACTIONSELECTED   0   Undefined   ${selectedarray[0]} 0">> $SAVEFILE
      
    else
    matcharray=()
        IFS=' ' read -a matcharray <<< "$MATCH"
      FRACTIONUNSELECTED=$(echo ${matcharray[0]} / $UNSELECTEDCOUNT | bc -l )
      ENRICHMENT=$(echo $FRACTIONSELECTED / $FRACTIONUNSELECTED | bc -l )

        echo "${selectedarray[1]} $FRACTIONSELECTED $FRACTIONUNSELECTED $ENRICHMENT ${selectedarray[0]} ${matcharray[0]}">> $SAVEFILE
  
    fi
done < $SELECTEDPROT

#sort the file appropriately and supply headers
#can only sort by enrichment if an unselected file is provided
if [ "$UNSELECTEDPROT" != "NO" ]
then
echo "Sequence    Selected_fraction Unselected_fraction   Ratio_change_in_selected    Sequence_count_selected Sequence_count_unselected   Total_sequence_count_selected: $SELECTEDCOUNT   Total_sequence_count_unselected: $UNSELECTEDCOUNT Unselected_Library: $UNSELECTED" > $SAVEPATH/$PFX.sortedbyenrichment.txt
sort -rk4 -n $SAVEFILE >> $SAVEPATH/$PFX.sortedbyenrichment.txt
fi
#sort by rank, regardless
echo "Sequence    Selected_fraction Unselected_fraction   Ratio_change_in_selected    Sequence_count_selected Sequence_count_unselected   Total_sequence_count_selected: $SELECTEDCOUNT   Total_sequence_count_unselected: $UNSELECTEDCOUNT Unselected_Library: $UNSELECTED" > $SAVEPATH/$PFX.sortedbyrank.txt
sort -rk5 -n $SAVEFILE >> $SAVEPATH/$PFX.sortedbyrank.txt

#remove non-essential files
rm -f $SAVEFILE
rm -f $SAVEPATH/$PFX.phage_display_list_protein.txt
rm -f $SAVEPATH/$PFX.phage_histogram_sorted_protein.txt

#6) Filter the analyzed files to exclude stopgain sequences and those with a read depth of lessthan specified

#filtered enriched population only if it exists
if [ "$UNSELECTEDPROT" != "NO" ]
then
perl $SCRIPT_DIR/Phage_display_filter_fasta.pl $FILTER $SAVEPATH/$PFX.sortedbyenrichment.txt $SAVEPATH/$PFX.phage_display_enrichmented_filtered.fa
fi
#filter by rank, regardless
perl $SCRIPT_DIR/Phage_display_filter_fasta.pl $FILTER $SAVEPATH/$PFX.sortedbyrank.txt $SAVEPATH/$PFX.phage_display_rank_filtered.fa



exit;

